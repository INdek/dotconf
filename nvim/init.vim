if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  au VimEnter * PlugInstall | source $MYVIMRC
endif

" Specify a directory for plugins
call plug#begin('~/.config/nvim/bundle')

" -- Visual --
Plug 'tomasr/molokai' " Theme

" -- Functional --
Plug 'christoomey/vim-tmux-navigator' " Tmux integration

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' } " Async search!
Plug 'junegunn/fzf.vim'

Plug 'dietsche/vim-lastplace' " Per file cursor position persistance

Plug 'tpope/vim-fugitive' " Git integration

Plug 'editorconfig/editorconfig-vim'


Plug 'terryma/vim-multiple-cursors'
Plug 'djoshea/vim-autoread'


Plug 'scrooloose/nerdcommenter'


" Language Server Autocomplete engine
Plug 'neoclide/coc.nvim', {'tag': '*', 'do': './install.sh'}

" -------- Languages ---------

" -- Shaders --
Plug 'tikhomirov/vim-glsl', { 'for': 'glsl' }

" -- CSS --
Plug 'ap/vim-css-color', { 'for': 'css' }

" -- Typescript --
Plug 'leafgarland/typescript-vim'

" -- Toml --
Plug 'cespare/vim-toml', { 'for': 'toml' }

" -- LLVM --
Plug 'rhysd/vim-llvm', { 'for': 'llvm' }

" -- Dart --
Plug 'dart-lang/dart-vim-plugin', { 'for': 'dart' }


" Initialize plugin system
call plug#end()

" --------------------------- Theme -----------------------------------------
colorscheme molokai

" --------------------------- General config ---------------------------------
" Leader
let mapleader = " "

" Relative line numbering with regular numbers on 0
set number
set relativenumber

" faster vim
set lazyredraw
set ttyfast

" dont show mode in cmd
set noshowmode

set diffopt=filler,vertical
set inccommand=nosplit

" Allow closing unsaved buffers
set hidden

" Disable mouse support
if has('mouse')
  set mouse=
endif

"set shortmess=filnxtTo0
" the above is the default, doesen't seem to work
set shortmess=filnxtToc

" Better substitution and searching
set incsearch
set hlsearch

" Spellchecking
set spelllang=en

" Backup
set backupdir=$HOME/.config/nvim/backupdir
set directory=$HOME/.config/nvim/backupdir

if !isdirectory(&backupdir)
    call mkdir(&backupdir, "p")
endif

" Undo history
set undofile   " Maintain undo history between sessions
set undolevels=10000         " How many undos to keep
set undoreload=100000        " Number of lines to save for undo

set undodir=$HOME/.config/nvim/undodir
if !isdirectory(&undodir)
    call mkdir(&undodir, "p")
endif


" --------------- CoC -------------

      " \  'coc-emmet',
      " \  'coc-highlight',
      " \  'coc-html',
      " \  'coc-dictionary',
      " \  'coc-emoji',
      " \  'coc-omni',
      " \  'coc-tag',
      " \  'coc-pairs',
      " \  'coc-java',
      " \  'coc-pyls',
      " \  'coc-snippets',
      " \  'coc-solargraph',
      " \  'coc-jest',
      " \  'coc-prettier',
      " \  'coc-tslint',
let s:coc_extensions = [
      \  'coc-css',
      \  'coc-json',
      \  'coc-yaml',
      \  'coc-eslint',
      \  'coc-rls',
      \  'coc-tsserver',
      \ ]


" call coc#add_extension(join(get(s:, 'coc_extensions', [])))


" Config  recomendations
" Smaller updatetime for CursorHold & CursorHoldI
set updatetime=300

" don't give |ins-completion-menu| messages.
set shortmess+=c

" always show signcolumns
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[c` and `]c` to navigate diagnostics
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)

" Remap for format selected region
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)

" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use `:Fold` to fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)


" Add diagnostic info for https://github.com/itchyny/lightline.vim
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'cocstatus', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'cocstatus': 'coc#status'
      \ },
      \ }



" Using CocList
" Show all diagnostics
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>



" ------------- Keymappings -----------

" Quicker window movement
let g:tmux_navigator_no_mappings = 1

nnoremap <silent> <C-Left> :TmuxNavigateLeft<cr>
nnoremap <silent> <C-Down> :TmuxNavigateDown<cr>
nnoremap <silent> <C-Up> :TmuxNavigateUp<cr>
nnoremap <silent> <C-Right> :TmuxNavigateRight<cr>

nnoremap <silent> <C-S-Down> <C-W>-
nnoremap <silent> <C-S-Up> <C-W>+
nnoremap <silent> <C-S-Left> <C-W><
nnoremap <silent> <C-S-Right> <C-W>>


" Esc esc clears the search
nnoremap <esc><esc> :noh<return><esc>


" Replace CtrlP
nnoremap <Leader>p :GFiles<cr>
" Better searching
nnoremap <Leader>/ :BLines<cr>
" Buffer switching
nnoremap <Leader>s :Buffers<cr>
" Search tags
nnoremap <Leader>t :Tags<cr>
" Ag search
nnoremap <Leader>f :Ag<cr>


" ------------------------- Commands

" Highlight trailing whitespace
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
au BufWinEnter * match ExtraWhitespace /\s\+$/
au InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
au InsertLeave * match ExtraWhitespace /\s\+$/
au BufWinLeave * call clearmatches()
function! TrimWhiteSpace()
    %s/\s\+$//e
endfunction


let g:multi_cursor_exit_from_visual_mode = 0
let g:multi_cursor_exit_from_insert_mode = 0

" Default highlighting (see help :highlight and help :highlight-link)
highlight multiple_cursors_cursor term=reverse cterm=reverse gui=reverse
highlight link multiple_cursors_visual Visual




" NerdComment

" Disable default mappings
let g:NERDCreateDefaultMappings = 0

" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1

let g:NERDDefaultAlign = 'left'

nnoremap <Leader>c :call NERDComment("n", "Toggle")<CR>
vnoremap <Leader>c :call NERDComment("n", "Toggle")<CR>



""" jump to next error
nnoremap <leader>ne :ll<CR>



" ------------------------------- C# -------------------------------------
au FileType cs setlocal shiftwidth=4 tabstop=4 expandtab


" ------------------------------- PHP -------------------------------------

au FileType scss setlocal shiftwidth=2 tabstop=2 expandtab
au FileType css setlocal shiftwidth=2 tabstop=2 expandtab
au FileType html setlocal shiftwidth=2 tabstop=2 expandtab
au FileType php setlocal shiftwidth=4 tabstop=4 expandtab

au FileType php setlocal nowrap

" ----------------------- JavaScript Config -------------------------------

au FileType javascript setlocal shiftwidth=2 tabstop=2 expandtab

" ----------------------- JavaScript Config -------------------------------

au FileType typescript setlocal shiftwidth=2 tabstop=2 expandtab
" au FileType typescript setlocal softtabstop=0 noexpandtab shiftwidth=8 tabstop=8 endofline


" --------------------------- Ruby/Rails config ---------------------------
au FileType ruby setlocal shiftwidth=2 tabstop=2 expandtab
au FileType eruby setlocal shiftwidth=2 tabstop=2 expandtab

" --------------------------- C Config ------------------------------------
" Treat .h files as c, none of this cpp rubbish
au BufRead,BufNewFile *.h,*.c set filetype=c

au FileType c setlocal shiftwidth=8 tabstop=8 noexpandtab

"let g:deoplete#sources.c = ['buffer', 'tag']

" --------------------------- CPP Config ----------------------------------

au FileType cpp setlocal shiftwidth=2 tabstop=2 expandtab

" --------------------------- Shader Config ----------------------------------
au! BufRead,BufNewFile *.glslf,*.glslv set filetype=glsl

" ---------------------------  TOML Config ----------------------------------
au! BufRead,BufNewFile Pipfile set filetype=toml

" --------------------------- Lua Config ----------------------------------
au! BufRead,BufNewFile rockspec setlocal filetype=lua
au FileType lua setlocal shiftwidth=2 tabstop=2 expandtab

" --------------------------- JSON Config ----------------------------------
au FileType json setlocal shiftwidth=2 tabstop=2 expandtab

au FileType json command! JsonFix :%! python -m json.tool

" --------------------------- YAML Config ----------------------------------
au FileType yaml setlocal shiftwidth=2 tabstop=2 expandtab

" --------------------------- XML Config ----------------------------------
au FileType xml setlocal shiftwidth=4 tabstop=4 expandtab

" --------------------------- GO Config ----------------------------------
au FileType gohtmltmpl setlocal shiftwidth=2 tabstop=2 expandtab

" -------------------------- Text Config ---------------------------------
au FileType markdown,mkd,textile,text setlocal spell


" ------------------------ Handlebars Config -------------------------------
au BufNewFile,BufRead *.Handlebars set syntax=html

" --------------- Dart
au BufRead,BufNewFile *.dart setlocal filetype=dart
let dart_format_on_save = 1
au FileType dart setlocal shiftwidth=2 tabstop=2 expandtab












" --------------------------- Statusline -------------------------------
" Clear statusline
set statusline=

set statusline+=%t\  " Current Filename only (no path)

"display a warning if fileformat isnt unix
set statusline+=%#warningmsg#
set statusline+=%{&ff!='unix'?'['.&ff.']':''}
set statusline+=%*

"display a warning if file encoding isnt utf-8
set statusline+=%#warningmsg#
set statusline+=%{(&fenc!='utf-8'&&&fenc!='')?'['.&fenc.']':''}
set statusline+=%*

set statusline+=%h      "help file flag
set statusline+=%r      "read only flag
set statusline+=%m      "modified flag

" display current git branch
"  set statusline+=%{fugitive#statusline()}

"display a warning if &et is wrong, or we have mixed-indenting
"set statusline+=%#error#
"set statusline+=%{StatuslineTabWarning()}
"set statusline+=%*

set statusline+=%{StatuslineTrailingSpaceWarning()}

"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*

"display a warning if &paste is set
set statusline+=%#error#
set statusline+=%{&paste?'[paste]':''}
set statusline+=%*




set statusline+=%=      "left/right separator
set statusline+=%y      "filetype
set statusline+=\ %c,   "cursor column
set statusline+=%l/%L   "cursor line/total lines
set statusline+=\ %P    "percent through file
set laststatus=2        " Always show status line

"return the syntax highlight group under the cursor ''
function! StatuslineCurrentHighlight()
    let name = synIDattr(synID(line('.'),col('.'),1),'name')
    if name == ''
        return ''
    else
        return '[' . name . ']'
    endif
endfunction

"recalculate the trailing whitespace warning when idle, and after saving
autocmd cursorhold,bufwritepost * unlet! b:statusline_trailing_space_warning

"return '[\s]' if trailing white space is detected
"return '' otherwise
function! StatuslineTrailingSpaceWarning()
    if !exists("b:statusline_trailing_space_warning")
        if search('\s\+$', 'nw') != 0
            let b:statusline_trailing_space_warning = '[\s]'
        else
            let b:statusline_trailing_space_warning = ''
        endif
    endif
    return b:statusline_trailing_space_warning
endfunction

"return '[&et]' if &et is set wrong
"return '[mixed-indenting]' if spaces and tabs are used to indent
"return an empty string if everything is fine
function! StatuslineTabWarning()
    if !exists("b:statusline_tab_warning")
        let tabs = search('^\t', 'nw') != 0
        let spaces = search('^ ', 'nw') != 0

        if tabs && spaces
            let b:statusline_tab_warning =  '[mixed-indenting]'
        elseif (spaces && !&et) || (tabs && &et)
            let b:statusline_tab_warning = '[&et]'
        else
            let b:statusline_tab_warning = ''
        endif
    endif
    return b:statusline_tab_warning
endfunction

"return a warning for "long lines" where "long" is either &textwidth or 80 (if
"no &textwidth is set)
"
"return '' if no long lines
"return '[#x,my,$z] if long lines are found, were x is the number of long
"lines, y is the median length of the long lines and z is the length of the
"longest line
function! StatuslineLongLineWarning()
    if !exists("b:statusline_long_line_warning")
        let long_line_lens = s:LongLines()

        if len(long_line_lens) > 0
            let b:statusline_long_line_warning = "[" .
                        \ '#' . len(long_line_lens) . "," .
                        \ 'm' . s:Median(long_line_lens) . "," .
                        \ '$' . max(long_line_lens) . "]"
        else
            let b:statusline_long_line_warning = ""
        endif
    endif
    return b:statusline_long_line_warning
endfunction

"return a list containing the lengths of the long lines in this buffer
function! s:LongLines()
    let threshold = (&tw ? &tw : 80)
    let spaces = repeat(" ", &ts)

    let long_line_lens = []

    let i = 1
    while i <= line("$")
        let len = strlen(substitute(getline(i), '\t', spaces, 'g'))
        if len > threshold
            call add(long_line_lens, len)
        endif
        let i += 1
    endwhile

    return long_line_lens
endfunction

"find the median of the given array of numbers
function! s:Median(nums)
    let nums = sort(a:nums)
    let l = len(nums)

    if l % 2 == 1
        let i = (l-1) / 2
        return nums[i]
    else
        return (nums[l/2] + nums[(l/2)-1]) / 2
    endif
endfunction
