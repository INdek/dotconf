.PHONY: bash ctags emacs tmux nvim bin git service desktop email irssi xfce vdirsyncer orage todo i3 freecad pip tmuxinator hg

bash:
	ln -s $(shell pwd)/bash/bashrc ~/.bashrc

ctags:
	ln -s $(shell pwd)/bash/ctags ~/.ctags

emacs:
	ln -s $(shell pwd)/emacs.d ~/.emacs.d

tmux:
	ln -s $(shell pwd)/tmux/tmux.conf ~/.tmux.conf

nvim:
	ln -s $(shell pwd)/nvim ~/.config/nvim

bin:
	mkdir -p ~/bin/
	#ln -s $(shell pwd)/bin/tmx ~/bin/tmx
go:
	mkdir -p ~/go

hg:
	ln -s $(shell pwd)/hg/hgrc ~/.hgrc
git:
	ln -s $(shell pwd)/git/git_template ~/.git_template
	ln -s $(shell pwd)/git/gitconfig ~/.gitconfig

service:
	mkdir -p ~/.config/systemd/user/
	cp $(shell pwd)/service/emacsd.service ~/.config/systemd/user/
	cp $(shell pwd)/service/offlineimap.service ~/.config/systemd/user/
	cp $(shell pwd)/service/setxkbmap.service ~/.config/systemd/user/
	cp $(shell pwd)/service/offlineimap.timer ~/.config/systemd/user/
	cp $(shell pwd)/service/notify-mail.service ~/.config/systemd/user/
	cp $(shell pwd)/service/an2linux.service ~/.config/systemd/user/
	cp $(shell pwd)/service/thunar.service ~/.config/systemd/user/
	cp $(shell pwd)/service/break_.service ~/.config/systemd/user/

	# This is necessary for an2linux to work over bluetooth
	sudo mv /etc/systemd/system/bluetooth.target.wants/bluetooth.service /etc/systemd/system/bluetooth.target.wants/bluetooth.tmp
	sudo cp $(shell pwd)/service/bluetooth.service /etc/systemd/system/bluetooth.target.wants/bluetooth.service

desktop:
	mkdir -p ~/.local/share/applications/
	ln -s $(shell pwd)/desktop/* ~/.local/share/applications/

email:
	ln -s $(shell pwd)/email/offlineimaprc ~/.offlineimaprc
	ln -s $(shell pwd)/email/offlineimap.py ~/.offlineimap.py
	ln -s $(shell pwd)/email/mutt ~/.mutt
	ln -s $(shell pwd)/email/muttrc ~/.muttrc
	ln -s $(shell pwd)/email/msmtprc ~/.msmtprc
	ln -s $(shell pwd)/email/notmuch-config ~/.notmuch-config

irssi:
	ln -s $(shell pwd)/irssi ~/.irssi
	cd irssi/certs && \
		find ./ -type f -name '*.gpg' | \
		while read -r line; do \
		$$(echo "$$line" | awk 'END{ split($$0,a,/\//); split(a[2],b,/.gpg/); print ("gpg2 --output " b[1] " --decrypt " a[2]) }'); \
		done

vdirsyncer:
	ln -s $(shell pwd)/vdirsyncer ~/.config/vdirsyncer

orage:
	ln -s $(shell pwd)/orage ~/.config/orage

todo:
	ln -s $(shell pwd)/todo ~/.todo
	ln -s $(shell which todo.sh) ~/bin/todo

pip:
	ln -s $(shell pwd)/pip ~/.pip

gdb:
	ln -s $(shell pwd)/gdb ~/.gdb
