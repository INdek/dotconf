;; -- save all backup tilde files to a diffrent directory as to not
;; -- clobber the filesistem with useless files
(defvar saves-dir (expand-file-name "~/.emacs.d/saves/"))
(setq backup-directory-alist (list (cons ".*" saves-dir)))
(setq auto-save-list-file-prefix saves-dir)
(setq auto-save-file-name-transforms `((".*" ,saves-dir t)))
(setq backup-by-copying t)

;; -- Don't jump when scrolling
(setq scroll-conservatively 10000)
(setq scroll-step 1)

;; -- Auto indent on a new line, this is disabled because
;; -- it might not be necessary anymore
;;(define-key global-map (kbd "RET") 'newline-and-indent)

;; -- enable system clipboard
(setq x-select-enable-clipboard t)
(setq mouse-drag-copy-region nil)

;; -- hide the startup message
(setq inhibit-startup-message t)

;; -- Allocate more memory between GC runs
(setq gc-cons-threshold (* 8192 8192))

;; -- Hilight the matching parenthesis
(show-paren-mode t)

;; -- Disable toolbars, menus and scoll bars
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

;; -- Disable blinking cursor
(blink-cursor-mode 0)

;; prevent line wrapping
(setq-default truncate-lines 1)

;; prevent a newline at the end of the file
(setq-default require-final-newline nil)

;; Show fringes to the left
(setq-default fringes-outside-margins t)

;; follow source controlled symlinks without asking
(setq vc-follow-symlinks t)

;; -- Similar to terminal
(global-set-key (kbd "C-S-V") 'clipboard-yank)
(global-set-key (kbd "C-S-C") 'clipboard-kill-ring-save)

;; Global decent keybindings for text size adjust
(global-set-key (kbd "C-0") '(lambda () (interactive) (text-scale-adjust  0)))
(global-set-key (kbd "C-+") '(lambda () (interactive) (text-scale-adjust  1)))
(global-set-key (kbd "C--") '(lambda () (interactive) (text-scale-adjust -1)))

;; show-paren
(setq-default show-paren-delay 0.01)

;;gpg support
(setenv "GPG_AGENT_INFO" (or (getenv "GPG_AGENT_INFO") "/run/user/1000/gnupg/S.gpg-agent"))
(setenv "SSH_AUTH_SOCK" (or (getenv "SSH_AUTH_SOCK") "/run/user/1000/gnupg/S.gpg-agent.ssh"))

(add-hook 'prog-mode-hook #'(lambda ()
			      ;; show unncessary whitespace
			      (setq show-trailing-whitespace 1)

			      ;; Highlight common keywords
			      (font-lock-add-keywords
			       nil '(("\\<\\(TODO\\(?:(.*)\\)?:?\\)\\>"  1 'warning prepend)
				     ("\\<\\(FIXME\\(?:(.*)\\)?:?\\)\\>" 1 'error prepend)))))

 (provide 'config-defaults)
