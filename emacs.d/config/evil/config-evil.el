(use-package evil
  :init

  ;; -- enable evil mode
  (evil-mode 1)

  ;; -- Don't mess up windows when splitting
  (setq evil-auto-balance-windows nil)

  ;; -- make words similar to vim
  (add-hook 'after-change-major-mode-hook
	    '(lambda ()
	       (modify-syntax-entry ?_ "w")))

  ;;  (setq evil-emacs-state-cursor nil)
  ;;  (setq evil-normal-state-cursor nil)
  ;;  (setq evil-insert-state-cursor nil)
  ;;  (setq evil-visual-state-cursor '("orange" box))
  ;;  (setq evil-replace-state-cursor '("red" bar))
  ;;  (setq evil-operator-state-cursor '("red" hollow))
  (setq evil-normal-state-tag    "N"
	evil-insert-state-tag    "I"
	evil-visual-state-tag    "V"
	evil-emacs-state-tag     "E"
	evil-operator-state-tag  "O"
	evil-motion-state-tag    "M"
	evil-replace-state-tag   "R")

  (evil-ex-define-cmd "b" 'ibuffer)
  (evil-ex-define-cmd "bl" 'previous-buffer)
  (evil-ex-define-cmd "bn" 'next-buffer)
  (evil-ex-define-cmd "at" 'ansi-term)
  (evil-ex-define-cmd "rel[oad]" 'revert-buffer)
  (evil-ex-define-cmd "wbd" '(lambda ()
			       (interactive)
			       (save-buffer)
			       (evil-delete-buffer)))

  (mapc (lambda (r) (evil-set-initial-state (car r) (cdr r)))
	'((compilation-mode       . normal)
	  (help-mode              . normal)
	  (message-mode           . normal)
	  (debugger-mode          . normal)
	  (image-mode             . normal)
	  (doc-view-mode          . normal)
	  (profile-report-mode    . emacs)))

  ;;fuck this package
  ;;  (use-package nlinum-relative
  ;;    :config
  ;;    (nlinum-relative-setup-evil)
  ;;    (add-hook 'prog-mode-hook 'nlinum-relative-mode)
  ;;    (setq nlinum-relative-redisplay-delay 0))
  ;;  (global-linum-mode)

  ;; TODO: Make this work
  ;; (defun evil-ex-define-cmd-local (cmd function)
  ;;   "Locally binds the function FUNCTION to the command CMD."
  ;;   (interactive)
  ;;   (unless (local-variable-p 'evil-ex-commands)
  ;;     (setq-local evil-ex-commands (copy-alist evil-ex-commands)))
  ;;   (evil-ex-define-cmd cmd function))
  (global-set-key (kbd "<menu>") 'evil-ex)

  (evil-global-set-key 'insert (kbd "<backspace>") 'delete-backward-char)
  :bind (

	 ("C-S-H" . evil-window-left)
	 ("C-S-J" . evil-window-down)
	 ("C-S-K" . evil-window-up)
	 ("C-S-L" . evil-window-right)

	 ("M-H" . evil-window-decrease-width)
	 ("M-J" . evil-window-decrease-height)
	 ("M-K" . evil-window-increase-height)
	 ("M-L" . evil-window-increase-width)))


(provide 'config-evil)
