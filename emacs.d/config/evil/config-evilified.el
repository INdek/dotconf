(defvar evilified-state--modes nil
"List of all evilified modes.")

(evil-define-state evilified
  "Evilified state.
 Hybrid `emacs state' with carrefully selected Vim key bindings.
 See spacemacs conventions for more info."
  :tag " <N'> "
  :enable (emacs)
  :message "-- EVILIFIED BUFFER --"
  :cursor box)

;; default key bindings for all evilified buffers
(define-key evil-evilified-state-map "SPC" 'leader-map)
(define-key evil-evilified-state-map "/" 'evil-search-forward)
(define-key evil-evilified-state-map ":" 'evil-ex)
(define-key evil-evilified-state-map "h" 'evil-backward-char)
(define-key evil-evilified-state-map "j" 'evil-next-visual-line)
(define-key evil-evilified-state-map "k" 'evil-previous-visual-line)
(define-key evil-evilified-state-map "l" 'evil-forward-char)
(define-key evil-evilified-state-map "n" 'evil-search-next)
(define-key evil-evilified-state-map "N" 'evil-search-previous)
(define-key evil-evilified-state-map "v" 'evil-visual-char)
(define-key evil-evilified-state-map "V" 'evil-visual-line)
(define-key evil-evilified-state-map "gg" 'evil-goto-first-line)
(define-key evil-evilified-state-map "G" 'evil-goto-line)
(define-key evil-evilified-state-map (kbd "C-f") 'evil-scroll-page-down)
(define-key evil-evilified-state-map (kbd "C-b") 'evil-scroll-page-up)
(define-key evil-evilified-state-map (kbd "C-e") 'evil-scroll-line-down)
(define-key evil-evilified-state-map (kbd "C-y") 'evil-scroll-line-up)
(define-key evil-evilified-state-map (kbd "C-d") 'evil-scroll-down)
(define-key evil-evilified-state-map (kbd "C-u") 'evil-scroll-up)
(define-key evil-evilified-state-map (kbd "C-z") 'evil-emacs-state)

(defmacro evilified-state-evilify (mode map &rest body)
  "Set `evilified state' as default for MODE.
BODY is a list of additional key bindings to apply for the given MAP in
`evilified state'."
  (let ((defkey (when body `(evil-define-key 'evilified ,map ,@body))))
    `(progn (unless ,(null mode)
              (unless (memq ',mode evilified-state--modes)
                (push ',mode evilified-state--modes))
              (unless (or (bound-and-true-p holy-mode)
                          (eq 'evilified (evil-initial-state ',mode)))
                (evil-set-initial-state ',mode 'evilified)))
            (unless ,(null defkey) (,@defkey)))))
(put 'evilified-state-evilify 'lisp-indent-function 'defun)

(provide 'config-evilified)
