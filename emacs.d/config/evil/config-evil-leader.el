(defvar leader-prefix-key-string "<SPC>"
  "String to use as leader prefix key.")

(defvar leader-prefix-key (kbd leader-prefix-key-string)
  "Key to activate the local leader map.")

(defvar leader-map (make-sparse-keymap)
  "Keymap for 'leader key' shortcuts.")

(evil-define-key 'normal global-map leader-prefix-key leader-map)

(defvar leader-individual-maps nil)

(defun leader-get-mode-map (mode)
  "Get leader map for the MODE, if no map is found return the default map."
  (let ((internal-mode-map (cdr (assoc mode leader-individual-maps))))
    (if internal-mode-map
	internal-mode-map
      leader-map)))


(defun leader-set-key-for-mode (mode key binding)
  "Define a KEY BINDING for a individual MODE map."
  (let ((internal-mode-map (cdr (assoc mode leader-individual-maps))))
    (unless internal-mode-map
      (setq internal-mode-map (make-sparse-keymap))
      (set-keymap-parent internal-mode-map leader-map)
      (push (cons mode internal-mode-map) leader-individual-maps))
    (define-key internal-mode-map key binding)
    (evil-define-key 'normal (current-local-map) leader-prefix-key (leader-get-mode-map major-mode))
    (evil-define-key 'visual (current-local-map) leader-prefix-key (leader-get-mode-map major-mode))
    ;; (evil-get-minor-mode-keymap 'normal mode)
    ))


(defun leader-register-which-key-for-mode (mode &rest list)
  (mapc #'(lambda (i)
	    (leader-set-key-for-mode mode (car i) (cadr i))
	    (which-key-add-major-mode-key-based-replacements
	      mode
	      (concat leader-prefix-key-string " " (car i))
	      (car (cddr i)))
	    ) list))


(provide 'config-evil-leader)
