(use-package evil-numbers
  :init
  (global-set-key (kbd "M-a") 'evil-numbers/inc-at-pt)
  (global-set-key (kbd "M-z") 'evil-numbers/dec-at-pt))

(provide 'config-evil-numbers)
