(use-package evil-visualstar
  :init
  (setq evil-visualstar/persistent t)
  (global-evil-visualstar-mode))

(provide 'config-evil-visualstar)
