(use-package build-helper
  :ensure nil
  :no-require t
  :init
  (build-helper-setup))

(defun ayy/build-helper-register-keys (major-map)
  (define-key major-map (kbd "<f5>") 'build-helper-re-run-run)
  (define-key major-map (kbd "S-<f5>") 'build-helper-run-run)
  (define-key major-map (kbd "<f6>") 'build-helper-re-run-build)
  (define-key major-map (kbd "S-<f6>") 'build-helper-run-build)
  (define-key major-map (kbd "<f7>") 'build-helper-re-run-test)
  (define-key major-map (kbd "S-<f7>") 'build-helper-run-test))

(provide 'config-build-helper)
