(use-package abbrev
  :ensure nil
  :diminish abbrev-mode)

(provide 'config-abbrev)
