(use-package undo-tree
  :diminish undo-tree-mode
  :bind ("C-u" . undo-tree-visualize))

(provide 'config-undo-tree)
