(defun toggle-maximize-buffer () "Maximize buffer"
  (interactive)
  (if (= 1 (length (window-list)))
      (jump-to-register '_)
    (progn
      (window-configuration-to-register '_)
      (delete-other-windows))))

(bind-key "c" #'toggle-maximize-buffer leader-map)

(which-key-add-key-based-replacements
  "SPC c" "Maximize buffer")

(provide 'config-maximize-buffer)
