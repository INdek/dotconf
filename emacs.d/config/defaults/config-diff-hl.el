(use-package diff-hl
  :init
  (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)
  (global-diff-hl-mode)


  :config
  (when (not (display-graphic-p))
    (diff-hl-margin-mode))


  (when (display-graphic-p)
    (fringe-mode 3)
    (add-hook 'minibuffer-setup #'(lambda ()
	       (set-window-fringes (minibuffer-window) 0 0 nil))))

  (diff-hl-flydiff-mode t))

(provide 'config-diff-hl)
