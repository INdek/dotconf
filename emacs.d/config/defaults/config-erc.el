(require 'erc-services)
(erc-services-mode 1)


(when (file-exists-p "~/.emacs.d/.ercpass")
  (load "~/.emacs.d/.ercpass")
  (setq erc-prompt-for-nickserv-password nil)
  (setq erc-nickserv-passwords       ;; setq this password on .ercpass
	`((freenode     (("INdek" . ,freenode-nick-pass))))))

(setq erc-nick '("INdek"))
(setq erc-autojoin-channels-alist
      '(("irc.mozilla.org" "#rust-embedded")))

;;http://edward.oconnor.cx/2007/09/freenode-cloaking-and-erc
;;(erc :server "irc.freenode.net" :port 6667 :nick erc-nick)

(provide 'config-erc)
