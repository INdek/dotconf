(defface powerline-evil-base-face
  '((t (:foreground "white" :inherit mode-line)))
  "Base face for powerline evil faces.")

(defface powerline-evil-normal-face
  '((t (:background "green" :inherit powerline-evil-base-face)))
  "Powerline face for evil NORMAL state.")

(defface powerline-evil-insert-face
  '((t (:background "blue" :inherit powerline-evil-base-face)))
  "Powerline face for evil INSERT state.")

;; (defface powerline-evil-normal-face
;;   '((t (:background "#7BC273" :inherit powerline-evil-base-face)))
;;   "Powerline face for evil NORMAL state.")

;; (defface powerline-evil-insert-face
;;   '((t (:background "#00B3EF" :inherit powerline-evil-base-face)))
;;   "Powerline face for evil INSERT state.")

(defface powerline-evil-visual-face
  '((t (:background "orange" :inherit powerline-evil-base-face)))
  "Powerline face for evil VISUAL state.")

(defface powerline-evil-operator-face
  '((t (:background "cyan" :inherit powerline-evil-operator-face)))
  "Powerline face for evil OPERATOR state.")

(defface powerline-evil-replace-face
  '((t (:background "red" :inherit powerline-evil-base-face)))
  "Powerline face for evil REPLACE state.")

(defface powerline-evil-motion-face
  '((t (:background "magenta" :inherit powerline-evil-base-face)))
  "Powerline face for evil MOTION state.")

(defface powerline-evil-emacs-face
  '((t (:background "violet" :inherit powerline-evil-base-face)))
  "Powerline face for evil EMACS state.")

(defun powerline-evil-face ()
  "Function to select appropriate face based on `evil-state'."
  (let* ((face (intern (concat "powerline-evil-" (symbol-name evil-state) "-face"))))
    (if (facep face) face nil)))

(defface mode-line-buffer-name-modified-active-face
  '((t (:foreground "orange" :inherit powerline-active2)))
  "Mode line buffer name face when buffer is active and modified")

(defface mode-line-buffer-name-modified-inactive-face
  '((t (:foreground "orange" :inherit powerline-inactive2)))
  "Mode line buffer name face when buffer is inactive and modified")

(defface mode-line-buffer-name-unmodified-active-face
  '((t (:inherit powerline-active2)))
  "Mode line buffer name face when buffer is active and unmodified")

(defface mode-line-buffer-name-unmodified-inactive-face
  '((t (:inherit powerline-inactive2)))
  "Mode line buffer name face when buffer is inactive and unmodified")

(defun mode-line-get-buffer-name-face (mod act)
  (intern (format "mode-line-buffer-name-%s-%s-face"
		  (if mod "modified" "unmodified")
		  (if act "active" "inactive"))))

(defun mode-line-set-visual ()
  (setq-default mode-line-format
		'("%e"
		  (:eval
		   (let* ((active (powerline-selected-window-active))
			  (face1 (if active 'powerline-active1 'powerline-inactive1))
			  (face2 (if active 'powerline-active2 'powerline-inactive2))
			  (buffer-face (mode-line-get-buffer-name-face (buffer-modified-p) active))
			  (separator-left (intern (format "powerline-%s-%s"
							  (powerline-current-separator)
							  (car powerline-default-separator-dir))))
			  (separator-right (intern (format "powerline-%s-%s"
							   (powerline-current-separator)
							   (cdr powerline-default-separator-dir))))
			  (lhs (list (powerline-raw evil-mode-line-tag (powerline-evil-face) 'l)
				     (powerline-raw " " (powerline-evil-face))
				     (funcall separator-left (powerline-evil-face) buffer-face)
				     (powerline-buffer-id buffer-face 'r)
				     (funcall separator-left buffer-face face1)
				     (powerline-raw
				      (if (and defining-kbd-macro evil-this-macro)
					  (format " Recording macro [%c]" evil-this-macro)
					"")
				      face1)))

			  (rhs (list (funcall separator-right face1 face2)
				     (powerline-major-mode face2 'l)
				     (powerline-raw " " face2)
				     (funcall separator-right face2 face1)
				     (powerline-raw "%l" face1 'l)
				     (powerline-raw " : " face1)
				     (powerline-raw "%c" face1 'r))))

		     (concat (powerline-render lhs)
			     (powerline-fill face1 (powerline-width rhs))
			     (powerline-render rhs)))))))

(defun mode-line-set-terminal ()
  (setq-default mode-line-format
		'("%e"
		  (:eval
		   (let* ((active (powerline-selected-window-active))
			  (face1 (if active 'powerline-active1 'powerline-inactive1))
			  (face2 (if active 'powerline-active2 'powerline-inactive2))
			  (buffer-face (mode-line-get-buffer-name-face (buffer-modified-p) active))
			  (lhs (list (powerline-raw evil-mode-line-tag (powerline-evil-face) 'l)
				     (powerline-raw " " (powerline-evil-face))
				     (powerline-buffer-id buffer-face 'r)
				     (powerline-raw
				      (if (and defining-kbd-macro evil-this-macro)
					  (format " Recording macro [%c]" evil-this-macro)
					"")
				      face1)))

			  (rhs (list (powerline-major-mode face1 'l)
				     (powerline-raw " " face1)
				     (powerline-raw "%l" face2 'l)
				     (powerline-raw " : " face2)
				     (powerline-raw "%c" face2 'r))))

		     (concat (powerline-render lhs)
			     (powerline-fill face1 (powerline-width rhs))
			     (powerline-render rhs)))))))


(if (display-graphic-p)
    (mode-line-set-visual)
  (mode-line-set-terminal))

(provide 'config-modeline)
