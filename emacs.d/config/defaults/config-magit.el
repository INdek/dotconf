(defun setup-magit-mode ()
  )

(use-package magit
  :init
  (add-hook #'magit-pre-display-buffer-hook #'setup-magit-mode)
  (add-hook #'magit-popup-mode-hook #'setup-magit-mode)
  (which-key-add-key-based-replacements "SPC G" "Magit")
  :config
  :bind(:map leader-map
	     ("G" . magit-status)
	     :map magit-mode-map
	     ("C-:" . magit-git-command)
	     (":" . evil-ex))

  :diminish auto-revert-mode)

(provide 'config-magit)
