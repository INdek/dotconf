;; TODO Make this work with visual selection
(use-package drag-stuff
  :init
  (define-key evil-visual-state-map (kbd "M-<up>") 'drag-stuff-up)
  (define-key evil-visual-state-map (kbd "M-<down>") 'drag-stuff-down)
  (define-key evil-visual-state-map (kbd "M-<left>") 'drag-stuff-left)
  (define-key evil-visual-state-map (kbd "M-<right>") 'drag-stuff-right)
  :diminish)

(provide 'config-drag-stuff)
