(use-package projectile
  :commands projectile-command-map
  :init
  (setq projectile-mode-line '(:eval (format " P[%s]" (projectile-project-name))))
  (setq projectile-enable-caching t)
  (use-package helm-projectile
    :init
    ;; TODO: Move this into a helm initalization directory
    (setq projectile-completion-system 'helm))

  (projectile-global-mode)
  (helm-projectile-on)

  :diminish projectile-mode
  :bind(:map leader-map
	     ("l" . helm-locate)
	     ("p" . projectile-command-map)))

(which-key-add-key-based-replacements
  "SPC l"        "Helm Locate"
  "SPC p"        "Projectile"
  "SPC p f"      "List files in project"
  "SPC p F"      "List files in all projects"
  "SPC p g"      "List files at point in the project"
  "SPC p 4 f"    "List files in project. Other window"
  "SPC p 4 g"    "List files at point in the project. Other window"
  "SPC p d"      "List directories in project"
  "SPC p !"      "Shell command" ;; on projectile root
  "SPC p &"      "Async shell command" ;; on projectile root
  "SPC p o"      "Run multi-occur on open buffers"
  "SPC p i"      "Invalidate project cache"
  "SPC p R"      "Regenerates the projects TAGS file."
  "SPC p j"      "Find tag in project's TAGS file."
  "SPC p T"      "List test files"
  "SPC p s g"    "Run grep on project"
  "SPC p r"      "Search and replace"
  "SPC p k"      "Kills all project buffers."
  "SPC p D"      "Opens root in dired."
  "SPC p e"      "Recently visited files"
  "SPC p E"      "Opens .dirs-local.el"
  "SPC p s s"    "Run ag on the project"
  "SPC p c"      "Run compile command"
  "SPC p P"      "Run test command"
  "SPC p a"      "Jump to file other extension"
  "SPC p t"      "Jump to test file"
  "SPC p 4 t"    "Jump to test file. Other window."
  "SPC p p"      "List known projects"
  "SPC p S"      "Save project buffers"
  "SPC p ESC"    "Last project buffer"
  "SPC p z"      "Adds file to project cache"
  "SPC p l"      "List files in dir"
  "SPC p v"      "Run vc-dir the project"
  "SPC p b"      "List open project buffers"
  "SPC p 4 d"    "Open project directory. Other window."
  "SPC p 4 a"    "Jump to file other extension. Other window"
  "SPC p 4 b"    "List open project buffers. Other window"
  )

;; SPC p 4 C-o  Display a project buffer in another window without selecting it.
;; M-- C-c p s g  Run grep on projectile-grep-default-files in the project.

(provide 'config-projectile)
