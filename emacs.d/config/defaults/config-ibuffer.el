(with-eval-after-load 'evil-ex
  (with-eval-after-load 'ibuffer
    (evil-set-initial-state 'ibuffer-mode 'normal)
    (evil-define-key 'normal ibuffer-mode-map
      (kbd "m") 'ibuffer-mark-forward
      (kbd "t") 'ibuffer-toggle-marks
      (kbd "u") 'ibuffer-unmark-forward
      (kbd "U") 'ibuffer-unmark-backward
      (kbd "=") 'ibuffer-diff-with-file
      (kbd "j") 'evil-next-line
      (kbd "k") 'evil-previous-line
      (kbd "l") 'ibuffer-visit-buffer
      (kbd "v") 'ibuffer-toggle-marks
      )
    )
  )


(provide 'config-ibuffer)
