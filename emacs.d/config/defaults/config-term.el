;; term
(defface term-color-black
  '((t (:foreground "#2E3436" :background "#272822")))
  "Unhelpful docstring.")
(defface term-color-red
  '((t (:foreground "#CC0000" :background "#272822")))
  "Unhelpful docstring.")
(defface term-color-green
  '((t (:foreground "#4E9A06" :background "#272822")))
  "Unhelpful docstring.")
(defface term-color-yellow
  '((t (:foreground "#C4A000" :background "#272822")))
  "Unhelpful docstring.")
(defface term-color-blue
  '((t (:foreground "#3465A4" :background "#272822")))
  "Unhelpful docstring.")
(defface term-color-magenta
  '((t (:foreground "#75507B" :background "#272822")))
  "Unhelpful docstring.")
(defface term-color-cyan
  '((t (:foreground "#06989A" :background "#272822")))
  "Unhelpful docstring.")
(defface term-color-white
  '((t (:foreground "#D3D7CF" :background "#272822")))
  "Unhelpful docstring.")
'(term-default-fg-color ((t (:inherit term-color-white))))
'(term-default-bg-color ((t (:inherit term-color-black))))

;; ansi-term colors
(setq ansi-term-color-vector
      [term term-color-black term-color-red term-color-green term-color-yellow
            term-color-blue term-color-magenta term-color-cyan term-color-white])

;; -- enable utf8 mode
(defun my-term-use-utf8 ()
  (set-buffer-process-coding-system 'utf-8-unix 'utf-8-unix))
(add-hook 'term-exec-hook 'my-term-use-utf8)

;; -- delete buffer after term exits
(defadvice term-sentinel (around my-advice-term-sentinel (proc msg))
  (if (memq (process-status proc) '(signal exit))
      (let ((buffer (process-buffer proc)))
        ad-do-it
        (kill-buffer buffer))
    ad-do-it))
(ad-activate 'term-sentinel)

;; -- force bash on term
(defvar my-term-shell "/bin/bash")
(defadvice ansi-term (before force-bash)
  (interactive (list my-term-shell)))
(ad-activate 'ansi-term)

(defun setup-term-mode ()
  (goto-address-mode)
  (company-mode -1)
  (undo-tree-mode -1)
  (linum-mode -1)


  (define-key term-raw-map (kbd "C-:") 'evil-ex)

  ;; -- proper paste support in terminal
  (define-key term-raw-map (kbd "C-S-V") (lambda ()
					   (interactive)
					   (process-send-string
					    (get-buffer-process (current-buffer))
					    (current-kill 0))))

  (define-key term-raw-map (kbd "C-<left>") (lambda () (interactive) (term-send-raw-string "\eb")))
  (define-key term-raw-map (kbd "C-<right>") (lambda () (interactive) (term-send-raw-string "\ef")))

  (defun term-send-function-key ()
    (interactive)
    (let* ((char last-input-event)
           (output (cdr (assoc char term-function-key-alist))))
      (term-send-raw-string output)))

  (defconst term-function-key-alist '((f1  . "\eOP")
                                      (f2  . "\eOQ")
                                      (f3  . "\eOR")
                                      (f4  . "\eOS")
                                      (f5  . "\e[15~")
                                      (f6  . "\e[17~")
                                      (f7  . "\e[18~")
                                      (f8  . "\e[19~")
                                      (f9  . "\e[20~")
                                      (f10 . "\e[21~")
                                      (f11 . "\e[23~")
                                      (f12 . "\e[24~")))
  (dolist (spec term-function-key-alist)
    (define-key term-raw-map
      (read-kbd-macro (format "<%s>" (car spec)))
      'term-send-function-key))

  )

(add-hook 'term-mode-hook 'setup-term-mode)

(with-eval-after-load "evil"
  ;; -- emacs term mode
  (evil-set-initial-state 'term-mode 'emacs))


(provide 'config-term)
