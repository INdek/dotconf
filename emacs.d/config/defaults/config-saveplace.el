(use-package saveplace
  :init
  (setq save-place-file "~/.emacs.d/saveplace")
  (setq-default save-place t))

(provide 'config-saveplace)
