(use-package which-key
  :init
  (which-key-setup-side-window-bottom)
  (setq which-key-paging-prefixes '("<SPC>"))
  (which-key-mode)
  :diminish which-key-mode)

(provide 'config-which-key)

