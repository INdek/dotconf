(defun warn-undefined-hydra ()
  (interactive)
  (message "A Hydra was not defined"))

(use-package hydra
  :bind(:map leader-map
	     ("SPC" . warn-undefined-hydra)))

(provide 'config-hydra)
