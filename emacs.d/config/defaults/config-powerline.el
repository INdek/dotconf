(use-package powerline
  :init
  (setq powerline-default-separator 'arrow))

(provide 'config-powerline)
