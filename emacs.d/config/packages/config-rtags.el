(use-package rtags
  :init
  ;; (setq rtags-autostart-diagnostics t)
  (setq rtags-completions-enabled t)
  (setq rtags-use-helm t)
  :defer t)

(defhydra hydra-rtags (:color blue :hint nil)
  "
^Symbols^                        ^Location Stack^   ^Fix it^
-------------------------------------------------------------------------------
_s_: Find symbol at point        _>_: Forward       _t_: Fix it at point
_r_: Find references at point    _<_: Backward      _T_: Fix it
_f_: Find File                                    _i_: Add include for symbol
_S_: Find symbol                 ^RDM^
_R_: Find refrences              _b_: Start process
_v_: Find virtuals at point
"
  ("s" rtags-find-symbol-at-point)
  ("r" rtags-find-references-at-point)
  ("f" rtags-find-file)
  ("S" rtags-find-symbol)
  ("R" rtags-find-references)
  ("v" rtags-find-virtuals-at-point)
  (">" rtags-location-stack-back)
  ("<" rtags-location-stack-forward)
  ("t" rtags-fix-fixit-at-point)
  ("T" rtags-fixit)
  ("i" rtags-get-include-file-for-symbol)
  ("b" rtags-start-process-unless-running)
  ("ESC" nil "Exit"))


(defun config--flycheck-rtags-setup ()
  (require 'flycheck-rtags)
  (flycheck-select-checker 'rtags)
  ;; RTags creates more accurate overlays.
  (setq-local flycheck-highlighting-mode nil)
  (setq-local flycheck-check-syntax-automatically nil))

(defun enable-rtags (flycheck company)
  (interactive)
  (unless (assoc 'arduino-mode rtags-supported-major-modes)
      (push 'arduino-mode rtags-supported-major-modes))
  (when company
    (push 'company-rtags company-backends))
  (when flycheck
    (config--flycheck-rtags-setup))
  (evil-local-set-key 'normal (kbd "M-.") 'rtags-find-symbol-at-point)
  (evil-local-set-key 'normal (kbd "M-,") 'rtags-location-stack-back)
  (leader-set-key-for-mode major-mode "t" 'hydra-rtags/body)
  (which-key-add-major-mode-key-based-replacements major-mode "SPC t" "RTags"))

(provide 'config-rtags)
