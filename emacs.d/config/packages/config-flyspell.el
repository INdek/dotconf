(defhydra hydra-flyspell
  (:color red
   :hint nil)
  "Flyspell"
  ("j" flyspell-goto-next-error            "Goto next error")
  ("k" flyspell-goto-previous-error        "Goto previous error")
  ("q" flyspell-auto-correct-previous-word "Correct last word")
  ("ESC" nil                               "Cancel"))

(use-package flyspell
  :init
  :defer t
  :diminish flyspell-mode)

(defun enable-flyspell (global)
  (interactive)
  (if global
      (flyspell-mode)
    (flyspell-prog-mode))
  (leader-set-key-for-mode major-mode "q" 'hydra-flyspell/body)
  (which-key-add-major-mode-key-based-replacements major-mode "SPC q" "Flyspell"))


;; move point to previous error
;; based on code by hatschipuh at
;; http://emacs.stackexchange.com/a/14912/2017
(defun flyspell-goto-previous-error (arg)
  "Go to arg previous spelling error."
  (interactive "p")
  (while (not (= 0 arg))
    (let ((pos (point))
	  (min (point-min)))
      (if (and (eq (current-buffer) flyspell-old-buffer-error)
	       (eq pos flyspell-old-pos-error))
	  (progn
	    (if (= flyspell-old-pos-error min)
		;; goto beginning of buffer
		(progn
		  (message "Restarting from end of buffer")
		  (goto-char (point-max)))
	      (backward-word 1))
	    (setq pos (point))))
      ;; seek the next error
      (while (and (> pos min)
		  (let ((ovs (overlays-at pos))
			(r '()))
		    (while (and (not r) (consp ovs))
		      (if (flyspell-overlay-p (car ovs))
			  (setq r t)
			(setq ovs (cdr ovs))))
		    (not r)))
	(backward-word 1)
	(setq pos (point)))
      ;; save the current location for next invocation
      (setq arg (1- arg))
      (setq flyspell-old-pos-error pos)
      (setq flyspell-old-buffer-error (current-buffer))
      (goto-char pos)
      (if (= pos min)
	  (progn
	    (message "No more miss-spelled word!")
	    (setq arg 0))
	(forward-word)))))


(provide 'config-flyspell)
;; (cond
;;  ((executable-find "aspell")
;;   (setq ispell-program-name "aspell"))
;;  ((executable-find "hunspell")
;;   (setq ispell-program-name "hunspell")
;;   (setq ispell-local-dictionary "en_US")
;;   (setq ispell-local-dictionary-alist
;;         '(("en_US" "[[:alpha:]]" "[^[:alpha:]]" "[']" nil ("-d" "en_US") nil utf-8))))
;;  (t (setq ispell-program-name nil)
;; (message "You need install either aspell or hunspell for ispell")))

