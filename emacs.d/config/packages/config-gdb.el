;; GDB Debugging
(setq
 ;; use gdb-many-windows by default
 gdb-many-windows t

 ;; Non-nil means display source file containing the main routine at startup
 gdb-show-main t)

(unless (executable-find "gdb")
  (message "GDB not found in exec-path"))

(provide 'config-gdb)
