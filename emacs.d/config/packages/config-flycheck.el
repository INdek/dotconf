(defhydra hydra-flycheck
  (:pre (progn (setq hydra-lv t) (flycheck-list-errors))
   :post (progn (setq hydra-lv nil) (quit-windows-on "*Flycheck errors*"))
   :color red
   :hint nil)
  "Flycheck"
  ("f"  flycheck-error-list-set-filter                            "Filter")
  ("j"  flycheck-next-error                                       "Next")
  ("k"  flycheck-previous-error                                   "Previous")
  ("gg" flycheck-first-error                                      "First")
  ("G"  (progn (goto-char (point-max)) (flycheck-previous-error)) "Last")
  ("ESC"  nil "Exit"))


(use-package flycheck
  :init
  ;; customize flycheck temp file prefix
  (setq-default flycheck-temp-prefix ".flycheck")

  (setq-default flycheck-indication-mode nil)
  :config

  ;; disable json-jsonlist checking for json files
  (setq-default flycheck-disabled-checkers
		(append flycheck-disabled-checkers
			'(json-jsonlist)))
  (setq-default flycheck-disabled-checkers
		(append flycheck-disabled-checkers
			'(javascript-jshint)))
  :defer t)

(defun enable-flycheck ()
  (interactive)
  (flycheck-mode)
  (leader-set-key-for-mode major-mode "Q" 'hydra-flycheck/body)
  (which-key-add-major-mode-key-based-replacements major-mode "SPC Q" "Flycheck"))

(provide 'config-flycheck)
