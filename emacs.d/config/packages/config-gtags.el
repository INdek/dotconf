(defhydra hydra-gtags (:color blue :hint nil)
  "
^Symbols^                      ^History^
-----------------------------------------------------------
_t_: Find tags                 _h_: Show tags stack
_T_: Find tags other window    _n_: Next tag in history
_r_: Find rtag                 _p_: Previous tag in history
_s_: Select tag
_x_: Xref find
_l_: List tags in this function
"
  ("r" helm-gtags-find-rtag)
  ("t" helm-gtags-find-tag)
  ("T" helm-gtags-find-tag-other-window)
  ("s" helm-gtags-select)
  ("x" xref-find-apropos)
  ("h" helm-gtags-show-stack)
  ("p" helm-gtags-previous-history)
  ("n" helm-gtags-next-history)
  ("l" helm-gtags-tags-in-this-function)
  ("ESC" nil "Exit"))



(use-package ggtags
  :init
  (use-package helm-gtags
    :init
    (setq-default
     helm-gtags-ignore-case t
     helm-gtags-auto-update t
     helm-gtags-use-input-at-cursor t
     helm-gtags-pulse-at-cursor t
     helm-gtags-suggested-key-mapping t)
    :diminish helm-gtags-mode)
  :defer t)

(defun enable-gtags ()
  (interactive)
  (push 'company-gtags company-backends)
  (helm-gtags-mode)
  (evil-local-set-key 'normal (kbd "M-,") 'helm-gtags-pop-stack)
  (evil-local-set-key 'normal (kbd "M-.") 'helm-gtags-dwim)
  (leader-set-key-for-mode major-mode "t" 'hydra-gtags/body)
  (which-key-add-major-mode-key-based-replacements major-mode "SPC t" "GTags"))

(provide 'config-gtags)
