(defun build-ctags ()
  (interactive)
  (ignore-errors
    (when ayy/etags-enabled
      (let ((root (projectile-project-root)))
	(let ((user-emacs-directory (file-name-directory user-init-file)))
	  (with-temp-buffer
	    (async-shell-command (concat "ctags -e -R -f " root "TAGS") t)))))))

(defun visit-project-tags ()
  (interactive)
  (let ((tags-file (concat (projectile-project-root) "TAGS")))
    (visit-tags-table tags-file)))

(defhydra hydra-etags (:color blue :hint nil)
  "
^Symbols^
-----------------------------
_s_: Select tag
_x_: xref search

_b_: Build tags list
"
  ("s" helm-etags-select)
  ("x" xref-find-apropos)
  ("b" build-ctags)
  ("ESC" nil "Exit"))

(defun enable-etags ()
  (interactive)
  (setq-local ayy/etags-enabled t)
  (add-hook 'after-save-hook 'build-ctags t)

  ;; don't bother me about updating tags
  (setq tags-revert-without-query 1)

  ;; case insensitive tags operations
  (setq tags-case-fold-search nil)

  (evil-local-set-key 'normal (kbd "M-,") 'xref-pop-marker-stack)
  (evil-local-set-key 'normal (kbd "M-.") 'xref-find-definitions)
  (evil-local-set-key 'normal (kbd "M->") 'xref-find-definitions-other-window)
  (leader-set-key-for-mode major-mode "t" 'hydra-etags/body)
  (which-key-add-major-mode-key-based-replacements major-mode "SPC t" "ETags"))

(provide 'config-etags)
