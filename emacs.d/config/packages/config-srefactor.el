(use-package srefactor
  :init
  (setq-default semantic-default-submodes nil)
  (require 'srefactor-lisp))

(defhydra hydra-srefactor (:color blue :hint nil)
  "
^SRefactor^
----------------------
_S_: Refactor at point
_o_: One Line
_m_: Format sexp
_d_: Format defun
_b_: Format buffer
"
  ("S" srefactor-refactor-at-point)
  ("o" srefactor-lisp-one-line)
  ("m" srefactor-lisp-format-sexp)
  ("d" srefactor-lisp-format-defun)
  ("b" srefactor-lisp-format-buffer)
  ("ESC" nil "Exit"))


;; semantic-idle-reschedule
(defun enable-srefactor ()
  (interactive)
  (if (or (eq major-mode 'emacs-lisp-mode) (eq major-mode 'lisp-interaction-mode))
      (leader-set-key-for-mode major-mode "S" 'hydra-srefactor/body)
    (semantic-mode 1)
    (leader-set-key-for-mode major-mode "S" 'srefactor-refactor-at-point)
    (when company-backends
      (setq company-backends (delete 'company-semantic company-backends))))

  (which-key-add-major-mode-key-based-replacements major-mode "SPC S" "Semantic Refactor")
  )

(provide 'config-srefactor)
