(use-package company-c-headers
  :init
  (setq company-c-headers-path-system '("/usr/include/SDL2/" "/usr/include/c++" "/usr/local/input" "/usr/include"))
  :defer t)

(defun enable-company-c-headers ()
  (interactive)
  (add-to-list 'company-backends 'company-c-headers))


(provide 'config-company-c-headers)
