(use-package company
  :init
  (require 'color) ;; -- needed for the theme
  ;; -- autocomplete as we type
  (setq company-idle-delay 0.3)
  ;; -- disable lowercase on all completions
  (setq company-dabbrev-downcase nil)
  ;; -- allow non completion characters after interaction with match
  (setq company-require-match nil)
  ;; Align tooltip annotations to the right
  (setq company-tooltip-align-annotations t)
  ;; Set a minimum length of 3 to begin completion
  (setq company-minimum-prefix-length 3)
  ;; -- custom theme
  (let ((bg (face-attribute 'default :background)))
    (custom-set-faces
     `(company-preview ((t (:inherit default :background ,(color-lighten-name bg 10)))))
     `(company-preview-common ((t (:inherit company-preview))))
     `(company-preview-search ((t (:inherit company-preview))))
     `(company-scrollbar-bg ((t (:background ,(color-lighten-name bg 20)))))
     `(company-scrollbar-fg ((t (:background ,(color-lighten-name bg 5)))))
     `(company-template-field ((t (:background "deep sky blue" :foreground "black"))))
     `(company-tooltip ((t (:inherit default :background ,(color-lighten-name bg 10)))))
     `(company-tooltip-selection ((t (:inherit font-lock-function-name-face :background ,(color-lighten-name bg 40)))))
     `(company-tooltip-common ((t (:inherit font-lock-constant-face))))
     `(company-tooltip-common-selection ((t (:inherit company-tooltip-common))))
     `(company-tooltip-annotation ((t (:inherit font-lock-keyword-face))))
     `(company-tooltip-annotation-selection ((t (:inherit company-tooltip-annotation :background ,(color-lighten-name bg 40)))))
     `(company-tooltip-mouse ((t (:foreground "black"))))
     ;; `(company-tooltip-search ((t (:background "brightwhite" :foreground "black"))))
     )
    )


  (use-package company-statistics)

  (use-package company-quickhelp
    :if (display-graphic-p)
    :init
    (setq pos-tip-foreground-color "#c5c8c6")
    (setq pos-tip-background-color "#3b3e40"))

  :bind (:map company-active-map
	      ("[up]" . company-select-previous)
	      ("[down]" . company-select-next)
	      ("\C-w" . nil)
	      :map company-mode-map
	      ("<C-SPC>" . company-complete-common))
  :defer t
  :diminish company-mode)

(defun enable-company (&optional quickhelp)
  (interactive)
  (when (boundp 'company-backends)
    (make-local-variable 'company-backends))
  (company-mode)
  (company-statistics-mode)
  (when quickhelp
    (company-quickhelp-mode)))

(provide 'config-company)
