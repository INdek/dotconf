(defun arduino-run ()
  (enable-flycheck)
  (enable-ag)
  (enable-company)
  (enable-srefactor)
  (enable-rtags t t)
  (evil-local-set-key 'normal [f1] (lambda () (interactive) (manual-entry (current-word))))
  (evil-local-set-key 'normal (kbd "M-RET") 'rtags-fixit))

(use-package arduino-mode
  :init
  (add-hook 'arduino-mode-hook #'arduino-run)
  (add-hook 'arduino-mode-hook #'rtags-start-process-unless-running)
  (setenv "ARDMK_DIR" "/usr/share/arduino/")
  (setenv "ARDUINO_DIR" "/usr/share/arduino/")
  (setenv "AVR_TOOLS_DIR" "/usr/"))

;; rtags
;; flymake

(defun arduino-create-makefile ()
  "Create a sample Makefile in the current directory."
  (interactive)
  (with-temp-buffer
    (insert "ARDUINO_QUIET\t= 1\n")
    (insert "ARDUINO_LIBS\t=\n")
    (insert "BOARD_TAG\t= leonardo\n")
    (insert "MONITOR_PORT\t= /dev/ttyACM0\n\n")
    (insert (format "include %s/Arduino.mk" (getenv "ARDMK_DIR")))
    (write-file "./Makefile")))

(defun arduino-open-monitor ()
  "Use the Makefile to open a serial monitor to the arduino board."
  (interactive)
  (compile "make monitor" t))

(provide 'config-lang-arduino)
