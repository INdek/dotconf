;; (use-package qml-mode
;;   :init
;;   (add-to-list 'auto-mode-alist '("\\.qml\\'" . qml-mode)))

(use-package company-qml
  :init
  (add-hook 'qml-mode-hook
            (lambda ()
  (enable-ag)
              (add-to-list 'company-backends 'company-qml)))
  :defer t)

(provide 'config-lang-qml)
