(defhydra hydra-rust (:color blue :hint nil)
  "
^Rust Cargo commands^
------------------------------------------------
_r_: Run          _i_: Init          _u_: Update
_x_: Run-example  _n_: New           _c_: Repeat
_b_: Build        _f_: Current-test  _e_: Bench
_l_: Clean        _s_: Search        _o_: Current-file-tests
_d_: Doc          _t_: Test          _m_: Fmt
                _k_: Check         _q_: Clippy
"
  ("e"   cargo-process-bench)
  ("b"   cargo-process-build)
  ("l"   cargo-process-clean)
  ("d"   cargo-process-doc)
  ("n"   cargo-process-new)
  ("i"   cargo-process-init)
  ("r"   cargo-process-run)
  ("x"   cargo-process-run-example)
  ("s"   cargo-process-search)
  ("t"   cargo-process-test)
  ("u"   cargo-process-update)
  ("c"   cargo-process-repeat)
  ("f"   cargo-process-current-test)
  ("o"   cargo-process-current-file-tests)
  ("m"   cargo-process-fmt)
  ("k"   cargo-process-check)
  ("q" cargo-process-clippy)
  ("ESC" nil "Exit"))

(defun rust-run ()
  (racer-mode t)
  (enable-ag)
  (enable-company t)
  (flycheck-rust-setup)
  (enable-flycheck)
  (evil-local-set-key 'normal (kbd "M-.") 'racer-find-definition)
  (evil-local-set-key 'normal [f1] #'racer-describe)
  (evil-local-set-key 'normal [f5] 'cargo-process-run)
  (evil-local-set-key 'normal [f6] 'cargo-process-build)
  (evil-local-set-key 'normal [f7] 'cargo-process-test)
  (leader-set-key-for-mode major-mode (kbd "<SPC>") 'hydra-rust/body)
  (which-key-add-major-mode-key-based-replacements major-mode "SPC SPC" "Rust"))

(use-package rust-mode
  :init
  (add-hook 'rust-mode-hook #'rust-run)
  :mode "\\.rs\\'"
  :defer t)

(use-package racer
  :config
  (setq racer-rust-src-path
	(concat
	 (replace-regexp-in-string "\n$" ""
				   (shell-command-to-string "rustc --print sysroot"))
	 "/lib/rustlib/src/rust/src"))
  (setq racer-cmd (executable-find "racer"))
  :if (executable-find "racer")
  :defer t)

;; disabled until its decent
;; (use-package flycheck-rust
;;   :if (executable-find "cargo")
;;   :defer t)

(use-package cargo
  :if (executable-find "cargo")
  :defer t)


(provide 'config-lang-rust)
