(defhydra hydra-json (:color blue :hint nil)
  "
^JSON^
----------------------
_b_: Pretty print buffer
_r_: Pretty print region
"
  ("b" json-pretty-print-buffer)
  ("r" json-pretty-print)
  ("ESC" nil "Exit"))

(defun json-run ()
  (enable-flycheck)
  (enable-ag)
  (enable-company)
  (leader-set-key-for-mode major-mode (kbd "<SPC>") 'hydra-json/body)
  (which-key-add-major-mode-key-based-replacements major-mode "SPC SPC" "JSON"))

(use-package json-mode
  :init
  (add-hook 'json-mode-hook #'json-run)
  :defer t)

(provide 'config-lang-json)
