(defun eslint-fix-file ()
  (interactive)
  (message "eslint --fixing the file" (buffer-file-name))
  (shell-command (concat "eslint --fix " (buffer-file-name))))

(defun eslint-fix-file-and-revert ()
  (interactive)
  (let ((after-save-hook (remove 'build-ctags after-save-hook)))
    (save-buffer))
  (eslint-fix-file)
  (revert-buffer t t))

(defun web-mode-run ()
  (interactive)
  (enable-flycheck)

  ;; use eslint with web-mode for jsx files
  (flycheck-add-mode 'javascript-eslint 'web-mode)

  (enable-ag)
  (enable-etags)
  (enable-company)
  (setq indent-tabs-mode nil))

(use-package web-mode
  :init
  (setq web-mode-enable-css-colorization t)
  ;; -- set js as jsx content
  (setq web-mode-content-types-alist '(("jsx" . "\\.js[x]?\\'")))
  (setq web-mode-enable-current-element-highlight t)
  (setq web-mode-enable-current-column-highlight t)
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-css-indent-offset 2)
  (setq web-mode-code-indent-offset 2)
  (setq web-mode-attr-indent-offset 2)
  (setq web-mode-enable-auto-quoting nil)

  (defadvice web-mode-highlight-part (around tweak-jsx activate)
    (if (equal web-mode-content-type "jsx")
	(let ((web-mode-enable-part-face nil))
	  ad-do-it)
      ad-do-it))

  (evil-ex-define-cmd "es[fix]" 'eslint-fix-file-and-revert)
  (add-hook 'web-mode-hook 'web-mode-run)

  :defer t
  :bind (:map web-mode-map
	  ("C-%" . web-mode-navigate))
  :mode (("\\.phtml\\'" . web-mode)
	 ("\\.tpl\\.php\\'" . web-mode)
	 ("\\.[agj]sp\\'" . web-mode)
	 ("\\.as[cp]x\\'" . web-mode)
	 ("\\.erb\\'" . web-mode)
	 ("\\.mustache\\'" . web-mode)
	 ("\\.djhtml\\'" . web-mode)
	 ("\\.jsx?\\'" . web-mode)
	 ("\\.js?\\'" . web-mode)
	 ("\\.php?\\'" . web-mode)
	 ("\\.html?\\'" . web-mode)
	 ("\\.css?\\'" . web-mode)))

(provide 'config-lang-js)
