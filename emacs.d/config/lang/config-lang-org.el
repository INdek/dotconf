(defhydra hydra-org (:color blue :hint nil)
  "
^ORG Mode^
----------------------
_e_: Export menu
"
  ("e" org-export-dispatch)
  ("ESC" nil "Exit"))

(defun org-run ()
  (enable-ag)
  (enable-flyspell t)
  (leader-set-key-for-mode major-mode (kbd "<SPC>") 'hydra-org/body)
  (which-key-add-major-mode-key-based-replacements major-mode "SPC SPC" "ORG"))

(add-hook 'org-mode-hook #'org-run)

(provide 'config-lang-org)
