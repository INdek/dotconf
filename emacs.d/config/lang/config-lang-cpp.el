(defun set-cpp-code-conventions ()
  (c-set-offset 'substatement-open 0)

  (setq c++-tab-always-indent t)
  (setq c-basic-offset 4)
  (setq c-indent-level 4)

  (setq tab-stop-list '(4 8 12 16 20 24 28 32 36 40 44 48 52 56 60))
  (setq tab-width 4)
  (setq indent-tabs-mode t))

(use-package cmake-ide
  :init
  (cmake-ide-setup)
  :defer t)

(use-package modern-cpp-font-lock)

(defun cpp-run ()
  (enable-flycheck)
  (enable-company)
  (push 'company-clang company-backends)
  (enable-rtags t t)
  (enable-ag)
  (enable-srefactor)
  (set-cpp-code-conventions)
  (modern-c++-font-lock-mode)
  ;; (evil-local-set-key 'normal [f6] 'cmake-ide-compile)
  (evil-local-set-key 'normal [f1] (lambda () (interactive) (manual-entry (current-word))))
  (evil-local-set-key 'normal (kbd "M-RET") 'rtags-fixit)
  (ayy/build-helper-register-keys c++-mode-map))

(add-hook 'c++-mode-hook 'cpp-run)
(add-hook 'c++-mode-hook 'rtags-start-process-unless-running)

(provide 'config-lang-cpp)
