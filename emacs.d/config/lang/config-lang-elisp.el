;;; describe this point lisp only
(defun describe-foo-at-point ()
  "Show the documentation of the Elisp function and variable near point.
	This checks in turn:
	-- for a function name where point is
	-- for a variable name where point is
	-- for a surrounding function call
	"
  (interactive)
  (let (sym)
    ;; sigh, function-at-point is too clever.  we want only the first half.
    (cond ((setq sym (ignore-errors
		       (with-syntax-table emacs-lisp-mode-syntax-table
			 (save-excursion
			   (or (not (zerop (skip-syntax-backward "_w")))
			       (eq (char-syntax (char-after (point))) ?w)
			       (eq (char-syntax (char-after (point))) ?_)
			       (forward-sexp -1))
			   (skip-chars-forward "`'")
			   (let ((obj (read (current-buffer))))
			     (and (symbolp obj) (fboundp obj) obj))))))
	   (describe-function sym))
	  ((setq sym (variable-at-point)) (describe-variable sym))
	  ;; now let it operate fully -- i.e. also check the
	  ;; surrounding sexp for a function call.
	  ((setq sym (function-at-point)) (describe-function sym)))))

(defhydra hydra-elisp (:color blue :hint nil)
  "
^Elisp^
------------
_s_: Suggest
_S_: Semantic Refactor
"
  ("s" suggest)
  ("S" hydra-srefactor/body)
  ("ESC" nil "Exit"))

(use-package suggest
  :defer t)

(defun elisp-run ()
  (enable-etags)
  (enable-flycheck)
  (enable-ag)
  (enable-srefactor)
  (enable-flyspell nil)
  (enable-company t)
  (push 'company-elisp company-backends)
  (evil-local-set-key 'normal [f1] 'describe-foo-at-point)
  (leader-set-key-for-mode major-mode (kbd "<SPC>") 'hydra-elisp/body)
  (which-key-add-major-mode-key-based-replacements major-mode "SPC SPC" "Elisp"))

(add-hook 'emacs-lisp-mode-hook 'elisp-run)

(provide 'config-lang-elisp)
