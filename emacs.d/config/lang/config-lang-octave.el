(defun octave-run ()
  (enable-company nil))

(use-package octave-mode
  :ensure nil
  :no-require t
  :config
  (setq-default octave-auto-indent t)
  (setq-default octave-auto-newline t)
  (setq-default octave-blink-matching-block t)
  (setq-default octave-block-offset 4)
  (setq-default octave-continuation-offset 4)
  (setq-default octave-continuation-string "\\")
  (setq-default octave-mode-startup-message t)
  (setq-default octave-send-echo-input t)
  (setq-default octave-send-line-auto-forward t)
  (setq-default octave-send-show-buffer t)
  (evil-set-initial-state 'inferior-octave-mode 'emacs)
  (add-hook 'octave-mode-hook #'octave-run)
  )


(provide 'config-lang-octave)
