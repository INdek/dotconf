(defvar markdown-ap-process-name "convert-from-md-to-html"
  "process name of a converter.")

(defvar markdown-ap-output-name "/tmp/markdown-ap-result.html"
  "Filename of converted html.")

(defvar markdown-ap-source-buffer nil
  "Only run timer on this buffer")

(defvar markdown-ap-idle-time 1
  "Seconds of convert waiting")

(defun markdown-ap-command (output-file-name)
  (format "require \"redcarpet\"

markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML)
while doc = gets(\"\\0\")
  doc.chomp!(\"\\0\")
  File.write(\"%s\", markdown.render(doc))
end
" output-file-name))

(defun markdown-ap--do-convert ()
  "Convert the source buffer into HTML and switch back to the original buffer."
  (let ((doc (buffer-substring-no-properties (point-min) (point-max)))
	(cb (current-buffer)))
    (when (eq (current-buffer) markdown-ap-source-buffer)
      (process-send-string markdown-ap-process-name (concat doc "\0"))
      (eww-open-file markdown-ap-output-name)
      (switch-to-buffer cb))))

(defun markdown-ap ()
  "Start a realtime markdown preview."
  (interactive)
  (let ((process-connection-type nil)
	(convert-command (markdown-ap-command markdown-ap-output-name)))
    (setq markdown-ap-source-buffer (current-buffer))
    (start-process markdown-ap-process-name nil "ruby" "-e" convert-command)
    (run-with-idle-timer markdown-ap-idle-time t 'markdown-ap--do-convert)
    (let ((cb (current-buffer)))
      (switch-to-buffer-other-window nil)
      (markdown-ap--do-convert)
      (switch-to-buffer cb))))

;;; markdown-ap is based on markdown-preview-eww


(defhydra hydra-markdown (:color blue :hint nil)
  "
^MarkDown^
------------------------
_p_: Enable autopreview mode
"
  ("p" markdown-ap)
  ("ESC" nil "Exit"))

(defun markdown-run ()
  (enable-ag)
  (enable-flyspell t)
  (leader-set-key-for-mode major-mode (kbd "<SPC>") 'hydra-markdown/body)
  (which-key-add-major-mode-key-based-replacements major-mode "SPC SPC" "MarkDown"))

;; if executable find "multimarkdown"
;; as of right now on fedora we have to manually compile
;; https://github.com/fletcher/MultiMarkdown-5
(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
	 ("\\.md\\'" . markdown-mode)
	 ("\\.markdown\\'" . markdown-mode))
  :init
  (use-package markdown-preview-eww)
  (setq markdown-command "multimarkdown")
  (add-hook 'markdown-mode-hook #'markdown-run)
  (add-hook 'gfm-mode-hook #'markdown-run))

(provide 'config-lang-md)
