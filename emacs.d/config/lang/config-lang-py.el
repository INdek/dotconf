(defun pyvenv-elpy-install-requirements ()
  "Install python requirements to the current virtualenv."
  (interactive)
  (unless pyvenv-virtual-env
    (error "Error: no virtualenv is active"))
  (let ((dest "*elpy-install-requirements-output*")
	(install-cmd (format "%s/bin/pip install --force '%%s'" pyvenv-virtual-env))
	(deps '("elpy" "jedi" "pyflakes" "pep8" "flake8" "importmagic" "yapf" "rope")))
    (generate-new-buffer dest)
    (mapcar
     #'(lambda (pkg)
	 (message (format install-cmd pkg))
	 (call-process-shell-command (format install-cmd pkg) nil dest)) deps)
    (call-process-shell-command
     (format "%s/bin/pip freeze" pyvenv-virtual-env) nil dest)
    (switch-to-buffer dest))
  (elpy-rpc-restart))


(defun pyvenv-find-venv-current-project ()
  "Return the path to the virtualenv in the current project or
nil if none is found."
  (let ((venv nil)
	(find-pattern "find %s -maxdepth 4 -path '*bin/activate'"))

    (if (elpy-project-root)
	(setq venv
	      (replace-regexp-in-string
	       "/bin/activate[ \t\n]*" ""
	       (shell-command-to-string
		(format find-pattern (elpy-project-root))))))

    (if (> (length venv) 0)
	(replace-regexp-in-string "//" "/" venv))

    ))

(defun pyvenv-activate-venv-current-project ()
  "Activate a virtualenv if one can be found in the current
project; otherwise activate the virtualenv defined in
`venv-default'. Also restarts the elpy rpc process."
  (interactive)
  (let ((venv (pyvenv-find-venv-current-project)))
    (if venv
	(progn
	  (pyvenv-activate venv)
	  (elpy-rpc-restart)
	  (message "Using %s" pyvenv-virtual-env))
      (message "could not find a virtualenv here"))))

(defun python-run ()
  (interactive)
  (enable-company t)
  (enable-flycheck)
  (enable-ag)
  (elpy-mode)
  (pyvenv-activate-venv-current-project)
  (leader-register-which-key-for-mode 'python-mode
				      '("r" elpy-refactor "Refactor")
				      '("R" elpy-rpc-restart "RPC Restart")))

(defun python-shell-run ())


(use-package elpy
  :defer t
  :mode ("\\.py\\'" . python-mode)
  :init
  (add-hook 'inferior-python-mode-hook #'python-shell-run)
  (evil-set-initial-state 'inferior-python-mode 'emacs)

  (add-hook 'python-mode-hook #'python-run)
  (evil-define-key 'visual python-mode-map (kbd "C-M-x") 'python-shell-send-region)
  (evil-define-key 'normal python-mode-map (kbd "C-M-x") 'python-shell-send-defun)
  :config
  (setq elpy-rpc-backend "jedi")
  (setq elpy-rpc-python-command "python3")
  )


(provide 'config-lang-py)
