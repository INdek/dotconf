(defun cmake-run ()
  (enable-flycheck)
  (enable-ag)
  (enable-company)
  (ayy/build-helper-register-keys cmake-mode-map)
  (evil-local-set-key 'normal [f1] #'cmake-help))

(add-hook 'cmake-mode-hook 'cmake-run)

(defun gmake-run ()
  (enable-flycheck)
  (enable-ag)
  (enable-company)
  (ayy/build-helper-register-keys makefile-gmake-mode-map))

(add-hook 'makefile-gmake-mode-hook 'gmake-run)

(provide 'config-lang-make)
