(defun matlab-run ()
  (enable-company nil)
  (enable-ag)
  (enable-flycheck)
  ;; (mlint-minor-menu t)
  (define-key matlab-mode-map (kbd "C-M-x") 'matlab-shell-run-region-or-line)
  (define-key matlab-mode-map (kbd "M-;") 'matlab-comment-region)
  (define-key matlab-mode-map (kbd "<f5>") 'matlab-shell-save-and-go)
  (define-key matlab-mode-map (kbd "M-a") 'evil-numbers/inc-at-pt)
  (define-key matlab-mode-map (kbd "M-z") 'evil-numbers/dec-at-pt)
  )

(defun matlab-shell-run ())

(use-package matlab-mode
  :defer t
  :mode ("\\.m\\'" . matlab-mode)
  :init
  (setq-default mlint-programs '("/opt/MATLAB/R2016b/bin/glnxa64/mlint"))
  (setq-default mlint-verbose t)
  (setq-default matlab-shell-command "/opt/MATLAB/R2016b/bin/matlab")
  (setq-default matlab-shell-command-switches '("-nodesktop -nosplash"))
  (evil-set-initial-state 'matlab-shell-mode 'insert)
  (add-hook 'matlab-shell-mode-hook #'matlab-shell-run)
  (add-hook 'matlab-mode-hook #'matlab-run)
  )


(provide 'config-lang-matlab)
