;;(setq debug-on-error t)
(require 'package)

(setq package-archives '(("melpa-stable" . "https://stable.melpa.org/packages/")
                         ("org" . "http://orgmode.org/elpa/")
                         ("gnu" . "http://elpa.gnu.org/packages/")
			 ("melpa" . "https://melpa.org/packages/")))

(package-initialize)

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)

(use-package exec-path-from-shell
  :init
  (exec-path-from-shell-initialize))

;; -- auto install all packages by default
(setq use-package-always-ensure t)

(add-to-list 'load-path "~/.emacs.d/config/")
(add-to-list 'load-path "~/.emacs.d/config/lang/")
(add-to-list 'load-path "~/.emacs.d/config/packages/")
(add-to-list 'load-path "~/.emacs.d/config/defaults/")
(add-to-list 'load-path "~/.emacs.d/config/evil/")
(add-to-list 'load-path "/usr/local/share/emacs/site-lisp/rtags/")

(setq custom-file "~/.emacs.d/custom.el")

;; Theme stuff
(use-package color-theme-sanityinc-tomorrow
  :ensure t
  :init
  (load-theme 'sanityinc-tomorrow-night t))
(require 'config-powerline)
(require 'config-modeline)

;; preload evil so that if anything else fails I can still use emacs
(require 'config-evil)
(require 'config-evilified)
(require 'config-evil-leader)
(require 'config-evil-numbers)
(require 'config-evil-visualstar)
(require 'config-evil-surround)

;; emacs default settings
(require 'config-defaults)
(require 'config-executables)

;; default packages -- autoloaded and global
(require 'config-hydra)
(require 'config-which-key)
(require 'config-abbrev)
(require 'config-undo-tree)
(require 'config-saveplace)
(require 'config-ibuffer)
(require 'config-term)
(require 'config-projectile)
(require 'config-maximize-buffer)
(require 'config-fullscreen)
(require 'config-magit)
(require 'config-erc)
(require 'config-drag-stuff)
(require 'config-diff-hl)
(require 'config-build-helper)
(require 'config-disable-mouse)

;; optional packages -- need to be loaded by a language
(require 'config-flyspell)
(require 'config-etags)
(require 'config-gtags)
(require 'config-gdb)
(require 'config-company)
(require 'config-company-c-headers)
(require 'config-flycheck)
(require 'config-rtags)
(require 'config-srefactor)
(require 'config-ag)

;; -- load all lang files
(require 'config-lang-c)
(require 'config-lang-cpp)
(require 'config-lang-js)
(require 'config-lang-py)
(require 'config-lang-qml)
(require 'config-lang-json)
(require 'config-lang-elisp)
(require 'config-lang-make)
(require 'config-lang-rust)
(require 'config-lang-md)
(require 'config-lang-octave)
(require 'config-lang-matlab)
(require 'config-lang-org)
(require 'config-lang-arduino)
